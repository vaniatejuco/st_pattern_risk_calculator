<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Risk Calculator</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/sketchy/bootstrap.min.css" rel="stylesheet" integrity="sha384-N8DsABZCqc1XWbg/bAlIDk7AS/yNzT5fcKzg/TwfmTuUqZhGquVmpb5VvfmLcMzp" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class=" px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4">Risk Calculator</h1>
        <p class="lead">Calculates the Lot Size for opening positions according to the Structural Target Trading Strategy</p>
    </div>
    <div class="row">
        <form class="col-lg-6 p-5" action="index.php" method="get">
            <div class="form-group">
                <label for="balance">Account Balance</label>
                <input type="number" class="form-control" id="balance" name="balance" placeholder="Your Account Balance">
            </div>
            <div class="form-group">
                <label for="risk" class="col-sm-2 col-form-label">Risk</label>
                <div class="col-sm-10">
                <input type="text" readonly class="form-control-plaintext" id="risk" name="risk" value="10%">
                </div>
            </div>
            <div class="form-group">
                <label for="account">Type of Account</label>
                <select class="form-control" id="account" name="account">
                <option value=1000>XM - Micro</option>
                <option value=100000>XM - Standard</option>
                <option value=100000>XM - Zero</option>
                </select>
            </div>
            <div class="form-group">
                <label for="balance">Leverage</label>
                <input type="number" class="form-control" id="leverage" name="leverage" value=200>
            </div>

            <div class="form-group">
                <label for="balance">Price Opened</label>
                <input type="number" step=".00001" class="form-control" id="price-open" name="price-open">
            </div>

            <div class="form-group">
                <label for="balance">Price - Stop Loss</label>
                <input type="number" step=".00001" class="form-control" id="price-stop" name="price-stop">
            </div>
            <input type="submit" class="btn btn-outline-primary">
        </form>
        <div class="col-lg-6 p-5">
            <?php
            if (isset($_GET["balance"])) {
                $risk = $_GET["balance"] * 0.10;
                $pip_dif = abs($_GET["price-open"] - $_GET["price-stop"]);
                $pip_away = $pip_dif * 10000;
                $l_position = $risk / ($pip_dif / $_GET["price-open"]);
                $lot = $l_position / $_GET["account"];
                $margin = $l_position / $_GET["leverage"];                     
                # code...
            } else {
                $risk = "N/A";
                $pip_dif = "N/A";
                $l_position = "N/A";
                $lot = "N/A";
                $margin = "N/A";
                $pip_away = "N/A";
            }
             ?>
            <h4>Risk : <?= round($risk,2)?></h4>
            <h4>Lot Size : <?= round($lot,2)?></h4>
            <h4>Margin : <?= round($margin,2)?></h4>
            <h4>Leveraged Position : <?= round($l_position,2)?></h4>
            <h4>Pip from Stop : <?= round($pip_away,2) . "pips"?></h4>


        </div>
    </div>
</div>
</body>
</html>
